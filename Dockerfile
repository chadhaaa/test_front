FROM node:16.15.1-alpine AS development

ENV NODE_ENV development

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 3000

RUN echo "jenkins ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

CMD [ "npm", "start" ]